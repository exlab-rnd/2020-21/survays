import React from 'react';
import {Survey} from 'survey-react';

export class ExecuteSurvey extends React.Component {

    state = {
        survey: '{"title":"survey_shenhav_or","pages":[{"name":"page1","elements":[{"type":"text","name":"question1","title":"what is youre name?"},{"type":"checkbox","name":"how old are you?","choices":[{"value":"item1","text":"20-25"},{"value":"item2","text":"26-30"},{"value":"item3","text":"31-40"}]},{"type":"dropdown","name":"where do you live?","choices":[{"value":"item1","text":"north"},{"value":"item2","text":"central"},{"value":"item3","text":"south"}]},{"type":"radiogroup","name":"What is important for you to have in the workplace you find?","description":"(You can mark several answers)","choices":[{"value":"item1","text":"high salary"},{"value":"item2","text":"Teamwork"},{"value":"item3","text":"Drink beer in the middle of the day"},{"value":"item4","text":"Work as little as possible"},{"value":"item5","text":"self satisfaction"}]},{"type":"boolean","name":"Is computer science what you dreamed of learning as a child?"},{"type":"comment","name":"Where do you see yourself in 10 years?"},{"type":"rating","name":"Are the courses taught in computer science interesting?","rateValues":[{"value":1,"text":"very much"},{"value":3,"text":"yes"},{"value":4,"text":"Slightly"},{"value":5,"text":"no"},{"value":6,"text":"not at all"}]},{"type":"boolean","name":"Do you have concerns about finding a job after you finish your degree?"},{"type":"comment","name":"If not computer science, what would you like to learn?"},{"type":"signaturepad","name":"youre signature"}],"title":"Personality survey"}]}'
    }

    ShowSurvey() {
    }

    render() {
        return (
            <div className='surveys-container'>
                <Survey.Survey json={this.state.survey} />
            </div>

        );
    }
}


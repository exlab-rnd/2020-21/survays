import React from 'react';
import './Survey.css';
import service from '../services/api';

export class Survey extends React.Component {


    constructor(props) {
        super(props);

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSurveyContent = this.handleSurveyContent.bind(this);
        this.submit = this.submit.bind(this);
        this.home = this.home.bind(this);
        this.executeSurvey = this.executeSurvey.bind(this);
        this.FlagIfSubmit = false;//flag if the user submit the survey

    }

    state = {
        name: '',
        survey: ''
    }

    handleNameChange(event) {
        this.setState({name: event.target.value});
    }

    handleSurveyContent(event) {
        this.setState({survey: event.target.value});
    }

    submit() {
        if (this.state.name === '' && this.state.survey === '') {
            this.FlagIfSubmit = false;
        } else this.FlagIfSubmit = true;

        service.SurveyService.addSurvey(this.state.name, this.state.survey)
            .then(response => {
                if(response === 'added'){
                    alert("Survey added successfully");
                    this.props.history.push('/')
                }
                console.log(response);
            })
    }

    canSubmit() {
        if ((this.state.name === '' && this.state.survey === '') || this.FlagIfSubmit) {
            return true;
        } else return false;

    }

    home() {
        if (this.canSubmit()) {
            this.props.history.push('/')
        } else {
            // eslint-disable-next-line no-restricted-globals
            var r = confirm('Dear User,\n' +
                'You have not save your survey.\n' +
                'Do you want to save it ?');
            if (r) {
                this.submit();
                this.home();
            } else {
                this.props.history.push('/')
            }
        }

    }

    executeSurvey() {
        this.props.history.push('/execute-surveys')
    }


    render() {
        const {name, survey} = this.state;
        const isEnabled = name.length > 0 && survey.length > 0;
        return (
            <div className='survey-container'>
                <h1 className='TitleAbove'>
                    <em>
                        Enter as you wish right away.</em></h1>
                {/********************/}
                <div className='survey-holder'>
                    {/**/}
                    <div className='surveyTitle'>
                        <label htmlFor="title"><h2>Name of survey:</h2></label>
                        <input id="title" placeholder='Title' type="text" value={this.state.name}
                               onChange={this.handleNameChange}/>
                    </div>
                    {/**/}
                        <div className='questionArea'>
                            <label htmlFor="question"><h2>Subject:</h2></label>
                            <textarea id="question" placeholder='Write something..' rows='2' value={this.state.survey}
                                      onChange={this.handleSurveyContent}/>
                        </div>
                    {/**/}
                    <div className='buttonsHolder'>
                        <button className='survey-buttons' disabled={!isEnabled} onClick={this.submit}>Submit</button>

                        <button className='survey-buttons' onClick={this.home}>Home</button>
                    </div>
                </div>
                {/********************/}
            </div>


        );
    }
}

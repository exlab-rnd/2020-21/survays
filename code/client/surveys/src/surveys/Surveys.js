import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import './Surveys.css';
import Paper from "@material-ui/core/Paper";
import animation from "../images/animation.png";
import service from "../services/api";

export class Surveys extends React.Component {

    constructor(props) {
        super(props);

        this.addNewSurvey = this.addNewSurvey.bind(this);
        this.action = this.action.bind(this);

    }

    action() {
        this.props.history.push('/execute-surveys');
    }

    componentDidMount() {
        service.SurveyService.GetAllSurevey().then((arr) => {
            if (arr.length > 0) {
                const obj = JSON.parse(arr);
                const res = [];
                for (const i in obj) {
                    res.push(obj[i]);
                }

                this.setState({
                    surveys: res
                });
                console.log(this.state.surveys);
            }

        });
    }


    state = {
        surveys: []
    }


    columns = [
        {field: 'name', headerName: 'Name'},
        {field: 'survey', headerName: 'Survey'},
        {field: 'creation_date', headerName: 'Creation Date'},
        {field: 'action', headerName: 'Actions'}

    ];

    rows = [
        {id: 1, name: 'test', survey: 'test', creation_date: 35}
    ];

    addNewSurvey(event) {
        this.props.history.push('/survey');
    }


    render() {
        return (
            <span className='surveys-container'>
                <span className='background'> </span>
                <em>
                        <span className='welcomeText'>
                            <h1>Build fully-customizable surveys,</h1>
                            <h1>forms and quizzes that seamlessly</h1>
                            <h1>integrate into your application</h1>
                            <h1 className='welcomeText2'>
                                <p>
                                And that’s only the start.
                                </p>
                            </h1>

                            <div className='buttonPadding'>
                        <button className="new-survey-button" onClick={this.addNewSurvey}>
                            <span>create survey</span>
                        </button>
                    </div>
                        </span>
                    <span className="animation">
                    <img src={animation} alt="animation" width="30%" height="30%"/>
                </span>
                </em>







                <div>
                    <TableContainer component={Paper}>
                        <Table aria-label="a dense table" className="survey-table">
                            <TableHead>
                                <TableRow className="head-row">
                                    {this.columns.map((column) => (
                                        <TableCell
                                            key={column.field}
                                            align="center"
                                        >
                                            {column.headerName}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.surveys.map((row) => (
                                    <TableRow hover key={row.fields.id}>
                                        <TableCell align="center">{row.fields.name}</TableCell>
                                        <TableCell align="center">{row.fields.survey.survey}</TableCell>
                                        <TableCell align="center">{row.fields.creation_date}</TableCell>
                                        <TableCell align="center">
                                            <button className="action" onClick={this.action}>
                                                <span>+</span>
                                            </button>
                                        </TableCell>

                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>

            </span>


        );
    }
}



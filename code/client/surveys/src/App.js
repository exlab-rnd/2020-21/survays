import React from 'react'
import './App.css';
import {Survey} from "./surveys/Survey";
import {Surveys} from "./surveys/Surveys";
import {ExecuteSurvey} from "./surveys/ExecuteSurvey";

import {BrowserRouter as Router, Redirect, Route, Switch} from "react-router-dom";
import Background from "./components/Background";
import Header from "./components/Header";
import NavigationBar from "./components/NavigationBar";
import Footer from "./components/Footer";
import NewComp from "./components/my-component/NewComp";


export default class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <Background/>
                    <Header/>
                    <NavigationBar/>
                    <Switch>
                        <Route path="/new-comp" component={NewComp}/>
                        <Route path="/survey" component={Survey}/>
                        <Route path="/surveys" component={Surveys}/>
                        <Route path="/execute-surveys" component={ExecuteSurvey}/>
                        <Route exact path="/"><Redirect to="/surveys"/>
                        </Route>
                    </Switch>
                    <Footer/>
                </div>
            </Router>
        );
    }
}

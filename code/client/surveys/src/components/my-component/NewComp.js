import UserProfile from "./UserProfile";
import ItemsList from "./ItemsList";
import ItemDetails from "./ItemDetails";

const NewComp = () => {
    return (
        <div>
            <UserProfile/>
            <ItemsList/>
            <ItemDetails/>
        </div>
    )
}

export default NewComp;

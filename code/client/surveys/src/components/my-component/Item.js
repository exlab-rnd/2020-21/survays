import "./ItemsList.css";
import { useHistory } from "react-router-dom";

const Item = ({name}) => {

    const history = useHistory();

    console.log('history', history);
    const clickItem = (event) => {
        console.log(`clicked ${name}`)
        history.push('surveys')
    }

    return (
        <div className={'item'} onClick={clickItem}>
            {name}
        </div>
    )
}

export default Item;

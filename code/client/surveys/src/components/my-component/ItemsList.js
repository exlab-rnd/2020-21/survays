import service from "../../services/api";
import {useState} from "react";
import "./ItemsList.css";
import Item from "./Item";

const ItemsList = () => {

    const [items, setItems] = useState([]);

    if (items.length === 0) {
        service.SurveyService.GetAllSurevey()
            .then(surveys => JSON.parse(surveys))
            .then(surveys => {
                setItems(surveys)
                console.log(surveys);
            })
    }

    return (
        <div className={'itemsListContainer'}>
            {
                items.map(item => {
                    return <Item name={item.fields.name}/>
                })
            }

            <select multiple>
                <option value={'1'}>option 1</option>
                <option value={'2'}>option 2</option>
                <option value={'3'}>option 3</option>
            </select>
        </div>
    )
}

export default ItemsList;

import React from 'react';
import './Header.css';
import logo from '../images/surveyslogo.png';


function Header() {
    // Import result is the URL of your image
    return (<div className="header">
            <img src={logo} alt="Logo" width="20%" height="10%"/>
        </div>
    );
}

export default Header;


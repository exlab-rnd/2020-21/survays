import React from 'react';
import './Footer.css';

function Footer() {
    return (
        <footer className="footer ">
            <div className="content-fix-width-center">
                <p>
            <span>
                Copyright © Aviram's Team 2021
                <p> </p>
            </span>
                    <span>
                All rights reserved. All trademarks or registered trademarks are property of their respective owners.
            </span>
                </p>
                <p>
                    If you have questions about the service or require additional functionality, send an e-mail to Aviram ;-).
                </p>
            </div>
        </footer>
    )

}

export default Footer;
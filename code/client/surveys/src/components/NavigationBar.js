import React from 'react';
import './NavigationBar.css';

function NavigationBar() {
    return (
        <ul>
            <li><a className="active" href="/">Home</a></li>
            <li><a href="/">Contact</a></li>
            <li><a href="/">About</a></li>
        </ul>
    )
}

export default NavigationBar;

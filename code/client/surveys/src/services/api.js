import Axios from 'axios'

const $axios = Axios.create({
    baseURL: '/api/',
    headers: {
        'Content-Type': 'application/json'
    }
})

$axios.interceptors.response.use(
    (response) => response,
    (error) => {
        console.error("got error")
        console.error(error)

        throw error;
    });


class SurveyService {
    static addSurvey(name, survey) {
        return $axios
            .post('survey/add', {name, survey})
            .then(response => response.data)
    }
    static GetAllSurevey(){
        return $axios
            .get('allsurvey/get', )
            .then(response => response.data)
    }


    static getSurveys(surveyid){
        return $axios
            .get(`survey/add/Version_1.0.1${surveyid}`)
            .then(response => response.data)

    }

    static ShowSurvey(survey) {
        return $axios
            .post('ExecuteSurvey/add', {survey})
            .then(response => response.data)
    }
}

const service = {
    SurveyService
}

export default service

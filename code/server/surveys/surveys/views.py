from rest_framework import status, viewsets
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from .models import Survey
from django.core import serializers

from .models import Survey


@api_view(['POST'])
@renderer_classes([JSONRenderer])
def add_survey(request):
    data = request.data
    new_survey = Survey(name=data['name'], survey=data['survey'])
    new_survey.save()
    return Response("added", status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_survey(request, surveyid):
    print(surveyid)
    survey = Survey.objects.get(id=surveyid)
    return Response(survey.id, status=status.HTTP_200_OK)


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def get_all_surveys(request):
    res = serializers.serialize('json', Survey.objects.all())
    return Response(res, status=status.HTTP_200_OK)

from django.contrib import admin
from django.urls import path

from . import views

api_prefix = 'api'

urlpatterns = [
    path(f'{api_prefix}/admin/', admin.site.urls),
    path(f'{api_prefix}/survey/add', views.add_survey),
    path(f'{api_prefix}/survey/get/<int:surveyid>', views.get_survey),
    path(f'{api_prefix}/allsurvey/get', views.get_all_surveys),
]

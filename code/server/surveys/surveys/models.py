
from django.db import models


class Survey(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    survey = models.JSONField(max_length=10000)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s: %s' % (self.name, self.survey)



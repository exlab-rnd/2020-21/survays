# Generated by Django 3.1.4 on 2020-12-05 20:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('survey', models.JSONField(max_length=10000)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]

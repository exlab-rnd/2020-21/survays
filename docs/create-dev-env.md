### Create server
* Prepare virtualenv
    * settings... -> SDKs -> add python sdk -> system interpreter -> select program files/python -> ok
    * settings... -> SDKs -> python 3.8 -> packages -> verify pip + setuptools
    * settings... -> SDKs -> add python sdk -> virtual env -> defaults (verified created folder)
    * settings... -> project -> python sdk -> select the virtual env
* use virtualenv (when no project)
    * `venv\Scripts\activate`
    * `pip install django`
    * `pip freeze > requirements.txt`
    * `deactivate`
* use virtualenv (after clone)
    * `venv\Scripts\activate`
    * `pip install -r requirements.txt`
    * `deactivate`
* create django project
    * cd code/server
    * django-admin startproject surveys
  
* db migration
    * remove `survey` schema
    * run sql script (schema + default user)
    * run django migration: `python manage.py migrate`

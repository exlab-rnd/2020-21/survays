CREATE DATABASE IF NOT EXISTS `survey`;

DROP USER 'surveys_admin'@'%';
-- password MD5('surveys_admin')
CREATE USER 'surveys_admin'@'%' IDENTIFIED WITH mysql_native_password BY '34d5c41a0f42a45ee55e696fe76aef5d';
GRANT ALL ON survey.* TO 'surveys_admin'@'%';
FLUSH PRIVILEGES;